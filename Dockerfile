FROM openjdk:11-jdk

WORKDIR /app

COPY ./target/ /app/target/

CMD ["java", "-jar", "/app/target/main-0.0.1-SNAPSHOT.jar"]